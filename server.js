const express = require("express");

const app = express();

const citas = require ("./citas.json");

app.use("/", express.static("public"));

app.get("/", (req, res) => {
    res.send("<h1>Benvingut a la API de CITES!</h1>");
});

app.get("/api/cita", (req, res) => {

    const citaRandom = Math.floor(Math.random() * citas.length);

    res.send(citas[citaRandom])
});

const port = process.env.PORT || 8080;
app.listen(port, () => console.log("Listening on port " + port));